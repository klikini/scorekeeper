#!/bin/bash
set -e

# Install Dart
echo "Installing Dart"
wget -q https://storage.googleapis.com/dart-archive/channels/stable/release/1.24.3/sdk/dartsdk-linux-x64-release.zip
unzip -q dartsdk-linux-x64-release.zip

# Go to dart project
cd src

# Build
echo "Building ScoreKeeper"
../dart-sdk/bin/pub get
../dart-sdk/bin/pub build

# Go to phonegap project
cd ..

# Clear old build
rm -rf www/*

# Move new build
mkdir www
mv ./src/build/web/* www/

# Set up correct script tags
sed -i "s/main.dart/main.dart.js/g" www/*.html
sed -i "s/application\/dart/text\/javascript/g" www/*.html
