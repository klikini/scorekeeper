#!/bin/bash
set -e

# Install JDK
echo "Installing JDK"
apt-get install -y default-jdk > /dev/null

# Install SDK tools
echo "Installing Android SDK tools"
wget -q https://dl.google.com/android/repository/commandlinetools-linux-6200805_latest.zip
unzip -q commandlinetools-linux-6200805_latest.zip
mkdir android-sdk
export ANDROID_HOME="$(pwd)/android-sdk"
echo "ANDROID_HOME=${ANDROID_HOME}"
mv tools "${ANDROID_HOME}/tools"
export PATH="${ANDROID_HOME}/tools/bin:$PATH"

# Install SDK
echo "Installing Android SDK"
yes | sdkmanager --sdk_root="${ANDROID_HOME}" "platforms;android-25"
ls "${ANDROID_HOME}"
export PATH="${ANDROID_HOME}/platform_tools/bin:$PATH"
