library ScoreKeeper;

import "dart:convert";
import "dart:html";
import "dart:js";

import "package:intl/intl.dart";

part "dart/player.dart";
part "dart/save.dart";
part "dart/score.dart";
part "dart/ui.dart";
part "dart/ui_lists.dart";
part "dart/ui_modal.dart";

void main() {
	Save.init();
	UI.init();
}
