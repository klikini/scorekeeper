part of ScoreKeeper;

enum Activity {INDEX, SCORING, SETTINGS}

class UI {
	// Date formatter
	static final DateFormat DATEFORMAT = new DateFormat("EEE MMM d, y");

	// Comma formatter
	static final NumberFormat COMMAFORMAT = new NumberFormat("#,###");

	// Material colors
	static final List<String> COLORS = [
		"red",
		"pink",
		"purple",
		"indigo",
		"blue",
		"cyan",
		"teal",
		"green",
		"lime",
		"yellow",
		"amber",
		"orange",
		"brown"
	];

	static void init() {
		switch(activity) {
			case Activity.INDEX:
			  // Main page
				UI_Lists.listScores();
				break;
			case Activity.SCORING:
				if (selectedScoreName == null) {
					// Creating a new score
					UI_Modal.promptString("Create Score", (String result){
						if (result != null) {
							Save.addScore(new Score(result));
							window.open("score.html?score=${Uri.encodeComponent(result)}", "_self");
						} else {
							// Cancelled
							window.open("index.html", "_self");
						}
					});
				} else {
					// Opening a score
					UI_Lists.listPlayers();

					// Listen for new player button
					querySelector("#button_newplayer").onClick.listen((_) {
						if (querySelectorAll("#string_prompt_modal").length == 0) {
							UI_Modal.promptString("Create Player", (String result) {
								if (result != null) {
									Save.getScore(selectedScoreName).addPlayer(new Player(result));
									UI_Lists.listPlayers();
								}
							});
						}
					});
				}
				break;
			case Activity.SETTINGS:
				// Link buttons
				querySelectorAll('a[rel="external"]').onClick.listen((MouseEvent event) {
					event.preventDefault();
					window.open((event.target as AnchorElement).href, "_system");
				});

				// Clear button
				querySelector("#button_clearscores").onClick.listen((_) {
					UI_Modal.promptString("Type 'DELETE' below", (String result) {
						if (result.trim().toUpperCase() == "DELETE") {
							Save.empty();
							toast("Data cleared");
						} else {
							toast("Invalid entry");
						}
					});
				});

				// JSON export
				querySelector("#button_jsonout").onClick.listen((_) {
					UI_Modal.displayString(Save.DATA, true);
				});

				// JSON import
				querySelector("#button_jsonin").onClick.listen((_) {
					UI_Modal.promptString("Paste JSON", (String json) {
						if (json != null) {
							Save.empty();
							Save.DATA = json;
							Save.readData();
							toast("Data imported.");
						}
					});
				});
				break;
		}

		// Prevent FAB from scrolling away
		Element fab = querySelector(".btn-floating.floating-bottomright");
		if (fab != null) {
			void updateFabPosition() {
				int height = (document.body.scrollTop + window.innerHeight) - fab.clientHeight - 24;
				fab.style.top = "${height.toString()}px";
			}

			window.onScroll.listen((_) => updateFabPosition());
			window.onResize.listen((_) => updateFabPosition());
		}
	}

	// Get current activity
	static Activity get activity {
		switch(querySelector("title").text) {
			case "ScoreKeeper":
				return Activity.INDEX;
			case "Score":
				return Activity.SCORING;
			case "Settings":
				return Activity.SETTINGS;
			default:
				return null;
		}
	}

	// Display a toast
	static void toast(String message) {
		int duration = message.length * 200; // 200ms per character
		context["Materialize"].callMethod("toast", [message, duration]);
	}

	// Get current score (if selected, otherwise null)
	static String get selectedScoreName {
		return Uri.base.queryParameters["score"];
	}

	// Creates icon elements
	static ImageElement getIcon(String icon, [String color = "white"]) {
		return new ImageElement()
			..src = "img/ic_${icon}_${color}_36px.svg"
			..attributes["icon"] = "";
	}

	// Returns a material color based on name
	static String getColor(String str) {
		// Get the first letter of the input string
		String firstLetter = "_";
		str = str.toUpperCase();
		RegExp regex = new RegExp(r"^[A-Z]");
		int index = str.indexOf(regex);
		if (index > -1) {
			firstLetter = str.substring(index, index + 1);
		}

		if (firstLetter == "_") {
			return "grey";
		} else {
			int codeUnit = firstLetter.codeUnitAt(0);	// character code in range 65-90
			int index0 = codeUnit - 65;					// character code in range 0-26
			int rounded = (index0 / 2).floor();			// rounded to lowest multiple of 2
			return COLORS[rounded];
		}
	}
}
