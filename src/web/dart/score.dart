part of ScoreKeeper;

class Score {
	// Game's identifier
	String name;

	// Players involved + their scores
	List<Player> players = [];

	// Date created
	DateTime date;

	// Creates a new score with a name and optional players/date
	Score(String name, [List<Player> players, DateTime date]) {
		this.name = name.trim();

		if (players != null) {
			this.players = players;
		}

		if (date == null) {
			this.date = new DateTime.now();
		} else {
			this.date = date;
		}
	}

	// Adds a player to the game
	void addPlayer(Player player, [bool skipDataWrite = false]) {
		if (players.where((Player p) => p.name == player.name).toList().length > 0) {
			print("Error: Player '${player.name}' is already in game $name");
		} else {
			players.add(player);
		}

		if (!skipDataWrite) {
			Save.writeData();
		}
	}

	// Removes a player from the game
	void removePlayer(String playerName) {
		if (players.where((Player p) => p.name == playerName).toList().length > 0) {
			players.removeWhere((Player p) => p.name == playerName);
		} else {
			print("Error: Player '$playerName' is not in game $name");
		}

		Save.writeData();
	}

	// Gets the score's color for the UI
	String get color => UI.getColor(name);

	// Returns the score and its players as a map
	Map get map {
		// Assemble player data
		List<Map> playerMaps = [];
		players.forEach((Player player) {
			playerMaps.add(player.map);
		});

		return ({
			"name": name,
			"players": playerMaps,
			"date": date
		});
	}

	// Returns the score as a string representing the map
	String toString() => map.toString();

	// Returns the score as a string in JSON format
	Object toJson() => {"name": "$name", "players": players, "date": "${date.toString()}"};
}
