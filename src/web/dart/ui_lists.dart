part of ScoreKeeper;

class UI_Lists {
  // Assemble the scores collection
  static bool listScores() {
    if (UI.activity != Activity.INDEX) {
      return false;
    }

    if (Save.scores.length > 0) {
      // Saved scores

      // Collection parent
      Element collection = new DivElement()..classes = ["collection"];

      // For every saved score...
      Save.scores.forEach((Score score) {
        // Display the colored icon
        Element icon = UI.getIcon("compare_arrows")..classes = ["circle", score.color];

        // Display the title
        Element title = new SpanElement()
          ..classes = ["title"]
          ..text = score.name;

        // Display the date + player count
        StringBuffer subtitleText = new StringBuffer()
          ..write(score.players.length.toString())
          ..write(" Player")
          ..write((score.players.length == 1) ? "" : "s")
          ..write(" on ")
          ..write(UI.DATEFORMAT.format(score.date));

        Element subtitle = new ParagraphElement()..text = subtitleText.toString();

        // Display a delete icon
        Element deleteIcon = UI.getIcon("delete")..classes = ["red-text"];
        Element delete = new AnchorElement()
          ..classes = [
            "delete",
            "secondary-content",
            "btn-flat",
            "btn-floating",
            "waves-effect",
            "waves-red",
            "waves-circle"
          ]
          ..append(deleteIcon)
          ..onClick.listen((MouseEvent event) {
            // Don't navigate to score
            event
              ..preventDefault()
              ..stopPropagation();
            // Prompt with modal
            UI_Modal.confirmDeletion(score);
          });

        // Create a list item
        Element item = new AnchorElement()
          ..classes = ["collection-item", "avatar"]
          ..append(icon)
          ..append(title)
          ..append(subtitle)
          ..append(delete)
          ..href = "score.html?score=${Uri.encodeComponent(score.name)}";

        // Add to container
        collection.append(item);
      });

      querySelector("#scores_list_container").children = [collection];
    } else {
      // No saved scores

      Element noticeLine1 = new HeadingElement.h3()
        ..classes = ["orange-text", "center"]
        ..setInnerHtml("<br>")
        ..appendText("No saved scores");

      Element noticeLine2 = new ParagraphElement()
        ..classes = ["center"]
        ..text = "Tap the green button at the bottom to start one.";

      querySelector("#scores_list_container").children = [noticeLine1, noticeLine2];
    }

    return true;
  }

  // Assemble the players collection
  static bool listPlayers() {
    if (UI.activity != Activity.SCORING) {
      return false;
    }

    Score score = Save.getScore(UI.selectedScoreName);

    if (score.players.length > 0) {
      // Saved scores

      // Collection parent
      Element collection = new UListElement()..classes = ["collection"];

      // For every saved score...
      score.players.forEach((Player player) {
        // Display the colored icon
        Element icon = UI.getIcon("person")..classes = ["circle", player.color];

        // Display the title
        Element title = new SpanElement()
          ..classes = ["title"]
          ..text = player.name;

        // Minus button
        Element minusButtonIcon = UI.getIcon("remove")..classes = ["red-text", "text-darken-4"];
        Element minusButton = new ButtonElement()
          ..classes = ["btn", "btn-flat", "btn-floating", "white", "waves-effect", "waves-light"]
          ..append(minusButtonIcon)
          ..onClick.listen((_) {
            player.subtractPoint();
            listPlayers();
          });

        // Plus button
        Element plusButtonIcon = UI.getIcon("add")..classes = ["green-text", "text-darken-4"];
        Element plusButton = new ButtonElement()
          ..classes = ["btn", "btn-flat", "btn-floating", "white", "waves-effect", "waves-light"]
          ..append(plusButtonIcon)
          ..onClick.listen((_) {
            player.addPoint();
            listPlayers();
          });

        // Number display
        Element numberDisplay = new SpanElement()..text = UI.COMMAFORMAT.format(player.points);

        // Number control container
        Element numberContainer = new DivElement()
          ..classes = ["score-display"]
          ..append(minusButton)
          ..append(numberDisplay)
          ..append(plusButton);

        // Add amount button
        Element addButton = new AnchorElement()
          ..classes = ["btn", "btn-flat", "btn-floating", "white", "waves-effect", "waves-light"]
          ..append(UI.getIcon("add_circle_outline", "black"))
          ..onClick.listen((_) {
            UI_Modal.promptInt("Add points", (int points) {
              if (points != null) {
                player.points += points;
                Save.writeData();
                listPlayers();
              }
            });
          });

        // Set to amount button
        Element setButton = new AnchorElement()
          ..classes = ["btn", "btn-flat", "btn-floating", "white", "waves-effect", "waves-light"]
          ..append(UI.getIcon("mode_edit", "black"))
          ..onClick.listen((_) {
            UI_Modal.promptInt("Set points", (int points) {
              if (points != null) {
                player.points = points;
                Save.writeData();
                listPlayers();
              }
            });
          });

        // Display a delete icon
        Element deleteIcon = UI.getIcon("delete")..classes = ["red-text"];
        Element delete = new AnchorElement()
          ..classes = [
            "btn-flat",
            "btn-floating",
            "waves-effect",
            "waves-red",
            "waves-circle"
          ]
          ..append(deleteIcon)
          ..onClick.listen((_) {
            UI_Modal.confirmDeletion(player);
          });

        Element rightContainer = new DivElement()
          ..classes = ["secondary-content"]
          ..append(addButton)
          ..append(setButton)
          ..append(delete);

        // Create a list item
        Element item = new LIElement()
          ..classes = ["collection-item", "avatar"]
          ..append(icon)
          ..append(title)
          ..append(numberContainer)
          ..append(rightContainer);

        // Add to container
        collection.append(item);
      });

      querySelector("#players_list_container").children = [collection];
    } else {
      // No saved scores

      Element noticeLine1 = new HeadingElement.h3()
        ..classes = ["orange-text", "center"]
        ..setInnerHtml("<br>")
        ..appendText("No player scores");

      Element noticeLine2 = new ParagraphElement()
        ..classes = ["center"]
        ..text = "Tap the green button at the bottom to create one.";

      querySelector("#players_list_container").children = [noticeLine1, noticeLine2];
    }

    // Update title bar
    querySelector("a.brand-logo").text = score.name;

    return true;
  }
}
