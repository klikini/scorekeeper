part of ScoreKeeper;

class Save {
	// LocalStorage key
	static String KEY = "sk_save";

	// LocalStorage reference
	static Storage get STORE => window.localStorage;

	// Returns the data from LocalStorage as a string
	static String get DATA => STORE[KEY];

	// Changes the data in LocalStorage
	static set DATA(String newData) => STORE[KEY] = newData;

	// Contains the scores as a list
	static List<Score> scores = [];

	// Creates a new Save
	static void init() {
		readData();

		// Newest first
		scores.sort((Score a, Score b) => b.date.compareTo(a.date));
	}

	// Loads the data from LocalStorage into memory
	static void readData() {
		if (STORE.containsKey(KEY)) {
			try {
				// Turn decoded maps back into score objects
				List<Map<String, dynamic>> scoresMaps = JSON.decode(DATA);
				scoresMaps.forEach((Map scoreMap) {
					DateTime scoreDate = DateTime.parse(scoreMap["date"]);
					Score workingScore = new Score(scoreMap["name"], null, scoreDate);

					// Turn players back into player objects
					(scoreMap["players"] as List<Map<String, dynamic>>).forEach((Map playerMap) {
						Player workingPlayer = new Player(playerMap["name"], playerMap["points"]);
						workingScore.addPlayer(workingPlayer, true);
					});

					// Add to score storage
					addScore(workingScore);
				});
			} catch(e) {
				print("Error decoding score data: $e");
			}
		}
	}

	// Writes the data from memory into LocalStorage
	static void writeData() {
		DATA = JSON.encode(scores);
	}

	// Saves a new score
	static void addScore(Score score) {
		if (scores.where((Score s) => s.name == score.name).toList().length > 0) {
			print("Error: Score '${score.name}' already exists");
		} else {
			scores.add(score);
		}

		writeData();
	}

	// Deletes an existing score
	static void removeScore(String scoreName) {
		if (scores.where((Score s) => s.name == scoreName).toList().length > 0) {
			scores.removeWhere((Score s) => s.name == scoreName);
		} else {
			print("Error: Score '$scoreName' does not exist");
		}

		writeData();
	}

	// Deletes all scores
	static void empty() {
		scores.clear();
		writeData();
	}

	// Returns the score with the given name
	static Score getScore(String name) {
		return scores.singleWhere((Score s) => s.name == name);
	}
}
