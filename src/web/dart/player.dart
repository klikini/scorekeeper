part of ScoreKeeper;

class Player {

	// Player's name
	String name;

	// Player's points
	int points;

	// Creates a new player with a name and optional points (0 if not set)
	Player(String name, [this.points = 0]) {
		this.name = name.trim();
	}

	// Adds 1 point (or any number of points)
	void addPoint([int number = 1]) {
		points += number.abs();
		Save.writeData();
	}

	// Subtracts 1 point (or any number of points)
	void subtractPoint([int number = 1]) {
		points -= number.abs();
		Save.writeData();
	}

	// Gets the player's color for the UI
	String get color => UI.getColor(name);

	// Returns the player as a map
	Map get map => {
		"name": name,
		"points": points
	};

	// Returns the player as a string representing the map
	String toString() => map.toString();

	// Returns the player as a string in JSON format
	Object toJson() => {"name": "$name", "points": points};
}
