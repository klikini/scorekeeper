part of ScoreKeeper;

class UI_Modal {
	// Display a modal prompting for name of score
	static bool promptString(String message, Function callback) {
		if (querySelectorAll("#string_prompt_modal").length > 0) {
			return false;
		}

		Element modal;

		// Modal header
		Element modalHeader = new HeadingElement.h4()
			..text = message;

		// Textbox
		InputElement text = new TextInputElement()
			..classes = ["col", "s8"]
			..placeholder = (message.toLowerCase().contains("create") ? "Name" : "");

		// Submit icon
		Element submitIcon = UI.getIcon("check");

		// Submit button
		Element submit = new ButtonElement()
			..classes = ["btn", "green", "col", "s3", "string-submit", "offset-s1", "modal-action", "modal-close", "wave-effect"]
			..append(submitIcon)
			..onClick.first.then((Event event) {
				event.preventDefault();
				Function.apply(callback, [text.value.trim()]);
			});

		// Form
		Element form = new FormElement()
			..classes = ["row"]
			..append(text)
			..append(submit);

		// Modal content
		Element modalContent = new DivElement()
			..classes = ["modal-content"]
			..append(modalHeader)
			..append(form);

		// Modal window
		modal = new DivElement()
			..classes = ["modal"]
			..append(modalContent)
			..id = "string_prompt_modal";

		// Open it
		document.body.append(modal);
		context["jQuery"].apply(["#string_prompt_modal"])
			.callMethod("openModal", [new JsObject.jsify({
				"dismissible": "false",
				"in_duration": "0",
				"out_duration": "300",
				"complete": () {
					modal.remove();
					Function.apply(callback, [null]);
				}
			})]);
		text.focus();

		return true;
	}

	// Display a modal prompting for number
	static bool promptInt(String message, Function callback) {
		if (querySelectorAll("#int_prompt_modal").length > 0) {
			return false;
		}

		Element modal;

		// Modal header
		Element modalHeader = new HeadingElement.h4()
			..text = message;

		// Textbox
		InputElement number = new NumberInputElement()
			..classes = ["col", "s8"]
			..value = "0";

		// Submit icon
		Element submitIcon = UI.getIcon("check");

		// Submit button
		Element submit = new ButtonElement()
			..classes = ["btn", "green", "col", "s3", "string-submit", "offset-s1", "modal-action", "modal-close", "wave-effect"]
			..append(submitIcon)
			..onClick.first.then((Event event) {
				event.preventDefault();
				try {
					int.parse(number.value);
					Function.apply(callback, [number.valueAsNumber.toInt()]);
				} catch(e) {
					print("Could not parse point value: $e");
				}
			});

		// Form
		Element form = new FormElement()
			..classes = ["row"]
			..append(number)
			..append(submit);

		// Modal content
		Element modalContent = new DivElement()
			..classes = ["modal-content"]
			..append(modalHeader)
			..append(form);

		// Modal window
		modal = new DivElement()
			..classes = ["modal"]
			..append(modalContent)
			..id = "int_prompt_modal";

		// Open it
		document.body.append(modal);
		context["jQuery"].apply(["#int_prompt_modal"])
			.callMethod("openModal", [new JsObject.jsify({
				"dismissible": "false",
				"in_duration": "0",
				"out_duration": "300",
				"complete": () {
					modal.remove();
					Function.apply(callback, [null]);
				}
			})]);
		number.focus();

		return true;
	}

	// Display a modal with a copyable string
	static void displayString(String str, [bool autoSelect = false]) {
		if (querySelectorAll("#string_display_modal").length > 0) {
			return;
		}

		Element modal;

		TextAreaElement text = new TextAreaElement()
			..classes = ["materialize-textarea", "text-monospace"]
			..text = str
			..readOnly = true;

		// Submit icon
		Element submitIcon = UI.getIcon("check");

		// Submit button
		Element submit = new ButtonElement()
			..classes = ["btn", "green", "btn-block", "modal-action", "modal-close", "wave-effect"]
			..append(submitIcon);

		// Form
		Element form = new FormElement()
			..append(text)
			..append(submit);

		// Modal content
		Element modalContent = new DivElement()
			..classes = ["modal-content"]
			..append(form);

		// Modal window
		modal = new DivElement()
			..classes = ["modal"]
			..append(modalContent)
			..id = "string_display_modal";

		// Open it
		document.body.append(modal);
		context["jQuery"].apply(["#string_display_modal"])
			.callMethod("openModal", [new JsObject.jsify({
				"complete": () {
					modal.remove();
				}
			})]);
		text.focus();

		if (autoSelect) {
			text.select();
		}
	}

	// Display a modal prompting for deletion of a score
	static bool confirmDeletion(dynamic instance) {
		if (instance is! Score && instance is! Player) {
			return false;
		}

		if (querySelectorAll("#modal_confirm_delete").length > 0) {
			return false;
		}

		Element modal;

		// Resets modal/buttons
		void complete() {
			// Re-show delete buttons
			querySelectorAll(".secondary-content").style.right = "";
			// Destroy modal
			modal.remove();
		}

		// Delete score
		void deleteScore() {
			Save.removeScore(instance.name);
			UI.toast("Score ${instance.name} deleted");
			UI_Lists.listScores();
		}

		// Delete player
		void deletePlayer() {
			Save.getScore(UI.selectedScoreName).removePlayer(instance.name);
			UI.toast("Player ${instance.name} deleted");
			UI_Lists.listPlayers();
		}

		// Hide all delete buttons
		querySelectorAll(".secondary-content").style.right = "-110px";

		// Modal header
		Element modalHeader = new HeadingElement.h4()
			..text = "Delete ${instance.name}?";

		// Yes button
		Element yesButtonIcon = UI.getIcon("check")
			..classes = ["left"];
		Element yesButton = new ButtonElement()
			..classes = ["btn", "red", "darken-1", "modal-action", "modal-close", "waves-effect"]
			..append(yesButtonIcon)
			..appendText("Yes")
			..onClick.first.then((_) {
				if (instance is Score) {
					deleteScore();
				} else if (instance is Player) {
					deletePlayer();
				}
			});

		// No button
		Element noButtonIcon = UI.getIcon("close")
			..classes = ["left"];
		Element noButton = new ButtonElement()
			..classes = ["btn", "green", "darken-1", "modal-action", "modal-close", "waves-effect"]
			..append(noButtonIcon)
			..appendText("No");

		// Modal content
		Element modalContent = new DivElement()
			..classes = ["modal-content"]
			..append(modalHeader)
			..append(yesButton)
			..appendText(" ")
			..append(noButton);

		// Modal window
		modal = new DivElement()
			..classes = ["modal", "bottom-sheet"]
			..append(modalContent)
			..id = "modal_confirm_delete";

		// Open it
		document.body.append(modal);
		context["jQuery"].apply(["#modal_confirm_delete"])
			.callMethod("openModal", [new JsObject.jsify({
				"complete": () {
					complete();
				}
			})]);

		return true;
	}
}
